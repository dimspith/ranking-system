package org.ggbattle.utils;

public final class MathFunctions {

    /**
     * Calculates the math sigmoid function with a parameter
     *
     * @param x - the input
     * @param a - the parameter
     * @return a/(1+e<sup>-x/a</sup>)
     */
    public static double sigmoid(double x, double a) {
        return a/(1 + Math.exp(-x/a));
    }

    /**
     * Calculates the math sigmoid function
     *
     * @param x - the input
     * @return 1/(1+e<sup>-x</sup>)
     */
    public static double sigmoid(double x) {
        return MathFunctions.sigmoid(x, 1);
    }

    //Prevent anyone from instantiating
    private MathFunctions() {}

}
