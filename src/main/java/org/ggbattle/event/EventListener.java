package org.ggbattle.event;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.ggbattle.RankingSystem;
import org.ggbattle.utils.MathFunctions;
import org.jetbrains.annotations.NotNull;

/**
 * Handles all events relevant to the {@linkplain RankingSystem} plugin
 *
 * @author Antonis Skourtis
 */
public final class EventListener implements Listener {

    private RankingSystem plugin;

    /**
     * Constructs the object with the given {@linkplain RankingSystem} instance
     *
     * @param plugin - the plugin instance
     */
    public EventListener(@NotNull RankingSystem plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player victim = event.getEntity();
        Player killer = event.getEntity().getKiller();

        if(killer != null) {

            if(victim.hasPermission(RankingSystem.Permissions.CASUAL) || victim.isOp())
                return;

            if(killer.hasPermission(RankingSystem.Permissions.CASUAL) || killer.isOp() || killer.getGameMode().equals(GameMode.CREATIVE))
                return;

            String vKey = victim.getUniqueId().toString();
            String kKey = killer.getUniqueId().toString();

            double vRanking = this.plugin.getRankings().getOrDefault(vKey, this.plugin.getDefaultRanking());
            double kRanking = this.plugin.getRankings().getOrDefault(kKey, this.plugin.getDefaultRanking());

            double dRanking = MathFunctions.sigmoid(vRanking-kRanking, this.plugin.getMaxRankingLoss());

            vRanking -= dRanking;
            kRanking += dRanking;

            this.plugin.getRankings().put(vKey, vRanking);
            this.plugin.getRankings().put(kKey, kRanking);

            victim.sendMessage(String.format(ChatColor.translateAlternateColorCodes('&',"&8Your ranking now is: &6%.2f&8(&c%s%.2f&8)"), vRanking, "-", dRanking));
            killer.sendMessage(String.format(ChatColor.translateAlternateColorCodes('&',"&8Your ranking now is: &6%.2f&8(&a%s%.2f&8)"), kRanking, "+", dRanking));
            //TODO: Config format
        }



    }

}
