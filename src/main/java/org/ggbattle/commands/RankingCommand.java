package org.ggbattle.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.ggbattle.RankingSystem;
import org.jetbrains.annotations.NotNull;

/**
 * Represents the ranking command
 *
 * @author Antonis Skourtis
 */
public final class RankingCommand implements CommandExecutor {

    private final @NotNull RankingSystem plugin;

    /**
     * Constructs the object with the given {@linkplain RankingSystem} instance
     *
     * @param plugin - the plugin instance
     */
    public RankingCommand(@NotNull RankingSystem plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof ConsoleCommandSender) {
            sender.sendMessage("These commands are intended for ingame use.");
            return true;
        }

        Player player = (Player)sender;

        if(args.length == 0) {
            double ranking = this.plugin.getRankings().getOrDefault(player.getUniqueId().toString(), this.plugin.getDefaultRanking());
            player.sendMessage(String.format(ChatColor.translateAlternateColorCodes('&',"&8Your ranking is: &6%.2f"), ranking)); //TODO: Config format
            return true;
        }

        //TODO: Handle with help page and return true?
        return false;
    }

}
