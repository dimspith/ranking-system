package org.ggbattle.papi;

import org.bukkit.entity.Player;
import org.ggbattle.RankingSystem;
import org.jetbrains.annotations.NotNull;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public final class RankingSystemPlaceholderExpansion extends PlaceholderExpansion {

    private final RankingSystem plugin;

    /**
     * Constructs the object with the given {@linkplain RankingSystem} instance
     *
     * @param plugin - the plugin instance
     */
    public RankingSystemPlaceholderExpansion(@NotNull RankingSystem plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist(){
        //Prevents the expansion on being unregistered after a reload
        return true;
    }

    @Override
    public boolean canRegister(){
        return this.plugin.getServer().getPluginManager().getPlugin("PlaceholderAPI") != null;
    }

    @Override
    public String getAuthor(){
        return this.plugin.getDescription().getAuthors().toString();
    }

    @Override
    public String getIdentifier(){
        return this.plugin.getDescription().getName();
    }

    @Override
    public String getVersion() {
        return this.plugin.getDescription().getVersion();
    }


    @Override
    public String onPlaceholderRequest(Player player, String identifier){
        if(player == null){
            return "";
        }

        if(identifier.equals("elo"))
            return String.format("%.2f", this.plugin.getRankings().getOrDefault(player.getUniqueId().toString(), this.plugin.getDefaultRanking()));
        

        return null;
    }
}
