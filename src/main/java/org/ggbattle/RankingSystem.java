package org.ggbattle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.java.JavaPlugin;
import org.ggbattle.commands.RankingCommand;
import org.ggbattle.event.EventListener;
import org.ggbattle.papi.RankingSystemPlaceholderExpansion;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

/**
 * Represents the RankingSystem plugin
 *
 * @author Antonis Skourtis
 */
public final class RankingSystem extends JavaPlugin {

    private final ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
    private final Map<String, Double> rankings = new HashMap<>();

    private File    rankingsFile;
    private double  defaultRanking;
    private double  maxRankingLoss;

    @Override
    public void onEnable() {
        //---------Config---------//
        this.saveDefaultConfig();
        if(!this.checkConfig())
            return;

        this.rankingsFile   = new File(this.getDataFolder(), this.getConfig().getString("file-system.file-name"));
        this.defaultRanking = this.getConfig().getDouble("ranking.default");
        this.maxRankingLoss = this.getConfig().getDouble("ranking.max-loss");
        //---------Config---------//

        //---------Rankings---------//
        if(!this.loadRankings()) {
            return;
        }
        //---------Rankings---------//

        //---------Commands/Listeners---------//
        this.getCommand("ranking").setExecutor(new RankingCommand(this));
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        //---------Commands/Listeners---------//

        //---------PAPI---------//
        this.getLogger().info("Hooking to PlaceholderAPI...");
        if(new RankingSystemPlaceholderExpansion(this).register()){
            this.getLogger().info("Hooked to PlaceholderAPI");
        } else {
            this.getLogger().info("Could not hook to PlaceholderAPI");
        }
        //---------PAPI---------//

        //---------AsyncTasks---------//
        int delay = this.getConfig().getInt("file-system.autosave-delay");
        this.scheduler.scheduleAtFixedRate(this::saveRankings, delay, delay, TimeUnit.SECONDS);
        //---------AsyncTasks---------//
    }

    @Override
    public void onDisable() {
        this.scheduler.shutdown();
        try {
            while(!this.scheduler.awaitTermination(1, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            this.scheduler.shutdownNow();
        }
        this.saveRankings();
        this.rankings.clear();
    }

    /**
     * Checks if the config is valid or not.
     *
     * @return true if config is valid false otherwise
     */
    private boolean checkConfig() {
        Configuration config   = this.getConfig();
        Configuration defaults = this.getConfig().getDefaults();

        Set<String> keys = defaults.getKeys(true);

        this.getLogger().info("Checking config...");
        for(String key : keys)
            if(!(config.contains(key) && config.get(key).getClass().equals(defaults.get(key).getClass()))) {
                this.getLogger().warning("Config is corrupt!"); //TODO: Show key at fault?
                return false;
            }
        this.getLogger().info("Config ok!");

        return true;
    }

    /**
     * Saves the rankings on the disc.
     *
     * @return true if the save was a success, false otherwise
     */
    private boolean saveRankings() {
        if(this.rankings.isEmpty())
            return true;
        this.getLogger().info("Saving rankings...");

        DumperOptions dumperOptions = new DumperOptions();
        dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        Yaml yamlManager = new Yaml(dumperOptions);

        this.rankingsFile.getParentFile().mkdirs();
        try(PrintWriter out = new PrintWriter(new FileOutputStream(this.rankingsFile))) {
            yamlManager.dump(this.rankings, out);
        } catch (FileNotFoundException e) {
            this.getLogger().warning("Could not save rankings!");
            this.getLogger().warning(e.getMessage());
            e.printStackTrace(System.err);
            return false;
        }
        this.getLogger().info(String.format("Rankings saved at: %s", this.rankingsFile));
        return true;
    }


    /**
     * Loads the rankings from the disc.
     *
     * @return true if the load was a success, false otherwise
     */
    private boolean loadRankings() {
        this.getLogger().info("Loading rankings...");
        if(!this.rankingsFile.exists()) {
            this.getLogger().info(String.format("Could not find file %s", this.rankingsFile));
            return true;
        }

        Yaml yamlManager = new Yaml();
        try(FileInputStream in = new FileInputStream(this.rankingsFile)) {
            this.rankings.putAll(yamlManager.load(in));
        } catch (IOException e) {
            this.getLogger().warning("Could not load rankings!");
            this.getLogger().warning(e.getMessage());
            e.printStackTrace(System.err);
            return false;
        }
        this.getLogger().info("Rankings ok!");
        return true;
    }


    /**
     * Gets the rankings
     * @return the rankings
     */
    public Map<String, Double> getRankings() {
        return this.rankings;
    }

    /**
     * Gets the default ranking
     * @return the default ranking
     */
    public double getDefaultRanking() {
        return this.defaultRanking;
    }

    /**
     * Gets the max ranking loss
     * @return the max ranking loss
     */
    public double getMaxRankingLoss() {
        return this.maxRankingLoss;
    }




    //-------Classes-------//
    /**
     * Holds all permissions for the plugin
     *
     * @author Antonis Skourtis
     */
    public final class Permissions {
        /**
         * Prevents the permission holder from being logged by the ranking system.
         */
        public static final String CASUAL = "RankingSystem.casual";

        //Prevent anyone from instantiating
        private Permissions() {}
    }
}
